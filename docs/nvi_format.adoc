= The NVI-netCDF4 convention for navigation file data, version 1.0
List of authors
:toc: left
:toclevels: 4
:doctype: book
:revnumber: 2.0
:xrefstyle: short
:source-highlighter: highlightjs
:highlightjsdir: highlight
:sectnumslevels: 4
:stem: latexmath
:eqnums:
:bibtex-file: references.bib
:bibtex-style: ices-journal-of-marine-science
:pdf-theme: pdf-theme.yml

// a future version of Asciidoctor will use the 'untitled' option to now show the heading
[colophon%untitled]
== Colophon
Ifremer

ZI Pointe du Diable.

CS 10070.

29280 PLOUZANÉ.



== Citation
Recommended format for purposes of citation:
....

_to be filled in once a new version is published_


DOI TO BE DEFINED HERE
....



:sectnums!:
== Foreword
:sectnums:

This report documents a convention for the storage of navigation data in netCDF4-formatted computer files. The intention is to provide a well-founded convention that is supported by multiple platform systems and multiple data processing software packages, with the aim of facilitating the use of these data for research and survey purposes. The name of this convention is NVI-netCDF4.

This document was originally developed by the Ifremer and Genavir Group on defining a data format for storage of navigation file acquired on the French Oceanographic Fleet platforms.

== Introduction

For a long time position and attitude data have been acquired on scientific platform to allow to geolocalize scientific dataset.

This document presents a convention for the storage and exchange of navigation data. It is sufficiently generic and flexible to contain all foreseeable types of platform position and attitude data, along with necessary metadata. It also serves as a statement of the minimum set of data and metadata required to use navigation for scientific data processing.

=== Background

All data linked to platform position and attitude of the platform will be called navigation data in this document.

Several purpose-built file formats exist to store and exchange data from navigation data. Some of them are linked to a constructor like Applanix POS data format or Kongsberg NavLab and some of them are dedicated to store position but miss fields for attitude or metadata (like gpx or kml file formats).

The aim of this document is to define a standard way to store information about navigation and attitude data of a platform to allow its use for scientific analysis of acquired data. Accordingly, an existing file-format definition has been utilized and what should be stored is specified. The requirements for such a file format definition were:

* ability to adequately represent the content and structure of navigation data and associated metadata;
* ability to describe navigation data for ship, underwater engine, trawled systems. Seismic system are excluded
* standardized, open-file format;
* fast random access to data stored in the file;
* ability to store multiple types of data (e.g. position and attitude);
* ability to store metadata (e.g. sensors settings);
* freely available and open libraries to read the data files into programming languages and technical computing environments (e.g. Java, C, C++, Python, Matlab, and R);
* self-describing file format;
* backwards compatibility upon modification of the file contents specification (i.e. old software/tools can still read relevant parts of a newer version);
* computer platform-independent;
* long-term support and extensive use in other scientific fields;


Other scientific communities that collect and produce voluminous amounts of data have addressed similar needs, resulting in the Hierarchical Data Format cite:[thehdfgroup2017], currently at version 5 (HDF5). This is the only file format that meets all the listed requirements and is utilized here for sonar data. There are two realizations of the HDF5 file format: (i) HDF5 itself and (ii) netCDF4 cite:[unidata2017], which is a subset of HDF5. Both are sufficient, but netCDF4 is more widely used and has slightly wider language support and implementation diversity. Accordingly, netCDF4 has been chosen.

Using a well-supported file format has the significant benefit that many data exploration, query, extraction, and analysis tools already read such files. This eliminates the need to develop and maintain sonar-specific file-reading software and facilitates the use of existing tools for data management, distribution, and analysis.

A distinction is commonly made between file formats designed for archiving data in the original form, formats designed for storing partially or fully processed data, and formats for data exchange. The NVI-netCDF4 convention is intended to be suitable for all of these.

=== Versioning

This document and the convention that it describes will change over time to implement enhancements and to correct errors and omissions. To accommodate this, the NVI-netCDF4 convention always requires a version number. This document has a separate version number, allowing for revised versions of the document to describe the same version of the convention.

The convention version number will always be included in the title of the document. The document version number will always be found in <<revision_history>>.



== The NVI convention

=== Introduction

NetCDF4 is a data model, application programming interface (API) library, and file format for storing and managing data, developed and maintained by Unidata. Unidata is part of the US University Corporation for Atmospheric Research (http://www2.ucar.edu/[UCAR]) Community Programs (http://www.ucp.ucar.edu/[UCP]) and funded primarily by the US National Science Foundation.

NVI-netCDF4 is a data and metadata convention for the storage of data for navigation data in netCDF4 formatted files. NVI-netCDF4 consists primarily of a naming convention and a data structure within the netCDF4 data model. This document is deeply inspired from the https://github.com/ices-publications/SONAR-netCDF4[SONAR-netCDF4] convention

Datasets can be added to NVI-netCDF4 files if they do not conflict with the NVI-netCDF4 datasets. If such additions are potentially of use to other users of the file format, it is recommended that they be proposed for inclusion in this or additional convention specifications.

Each NVI-netCDF4 file is intended to store data from one single rigid platform. The storage of data from multiple platform or articulated platforms is not in the scope of the convention.

=== Hierarchical structure

NetCDF4 has two main organizational concepts: (i) the variable, which can contain a variety of data structures, and (ii) groups, being a collection of variables. Groups and variables can have attributes attached to them. Groups can be arranged into a hierarchy. NVI-netCDF4 divides sonar data into a set of hiearchical netCDF4 groups:

.. */ -* top-level or root of the hierarchy and contains metadata about the NVI-netCDF4 file format;
.. *Platform –* contains information about the platform about each sensor. It may also contains raw data of the sensor at its acquisition time but this is not in the scope of definition of this document.
.. *Provenance –* contains metadata about how the SONAR-netCDF4 version of the data were obtained and processed;
.. *Navigation –* contains the main navigation and attitude for the platform, it contains acquired and processed navigation data and interpretation masks like quality factors;

These groups contain variables and variable attributes with prescribed names and contents.

=== Obligations and missing data

Some variables and attributes in SONAR-netCD4 are mandatory; these form the minimal set of data required to use data. The remaining variables and attributes have various levels of optionality and provide enhanced context and information about the data. The obligations are:

. *M*: mandatory
. *MA*: mandatory if applicable or available
. *R*: recommended
. *O*: optional

Any non-mandatory variables can be absent from a NVI-netCDF4 file. If a variable is mandatory, it must be present and must contain data. The set of mandatory variables and attributes has been chosen so that sonar systems can directly generate NVI-netCDF4 conforming files without needing survey, experiment, or cruise-specific data.

The _FillValue attribute should be used to indicate missing data in variables. For floating point values, the IEEE 754 not-a-number (NaN) is the preferred fill value as this is convenient for commonly-used analysis packages (e.g. Python, Matlab, R).

=== Units

All relevant variables and attributes in NVI-netCDF4 files are required to have a textual netCDF4 attribute with the name “units” that specifies the units. The International System of Units (SI) is used. For simplicity, the data format mandates the use of particular units and their textual form, as per the definitions and conventions of the UDUNITS-2 package .

==== Time units
Even though NetCDF and the UDUNITS-2 part of the CF conventions allow for the specification any valid time unit, to prevent any extra complication in software that reads the NVI-netCDF4 files, only one time units are supported in this convention.

* nanoseconds since 1970-01-01 00:00:00Z

The choice of nanoseconds is because sonar system timestamp precision can be less than one millisecond. Other forms of time units, such as 100 nanoseconds, are not well supported by the various tools that read CF-formatted time units, and nanoseconds units is retained.


=== Datatypes

Each item has a suggested datatype, chosen to have sufficient range and precision to contain the expected data. Alternative datatypes can be used if necessary, but are discouraged. The “string” type should contain text in the UTF-8 encoding and should be treated as case-sensitive during any comparisons. Enumerated datatypes are used for some of the controlled vocabularies.

=== Vocabularies

The contents of some variables and attributes are restricted to defined vocabularies. These are listed or referenced where required. Some of the vocabularies have been represented as netCDF4 enumeration data types and some using the CF flag_values convention. By default the controlled vocabulary used will be used,  by order of priority, from the https://cfconventions.org/[CF-Convention], The https://wiki.esipfed.org/Category:Attribute_Conventions_Dataset_Discovery[ACDD] convention and the http://seadatanet.maris2.nl/v_bodc_vocab_v2)[SeadDataNet] convention

=== File-naming convention

NVI-netCDF4 files should always end with a “.nc” suffix to indicate that they are a netCDF file. It is recommended that the filename should sort alphanumerically into chronological order (e.g. date and time of the first ping in the file; thus: YYYYMMDD-HHMMSS.nc). This facilitates file management and use in analysis systems.
To allow visual inspection in directory it is recommanded to identify  NVI-netCDF4 files with a ".nvi.nc" suffix




=== Coordinate systems
[[coordinate_systems]]

This section provides an overview of the coordinate systems use to physically locate the position of an instrument when measured from a moving or stationary platform.

The position are given as relative to the Platform Coordinate System (PCS) with X pointing forward, Y pointing to the starboard side (or right side when looking along X direction) and Z pointing downside.
Angles (roll, pitch, yaw) are oriented relatively to this coordinate system

There is also a geographic coordinate system that provides for location of the platform on Earth and also the height above a datum. The platform heading variable (degrees clockwise from north) can be used to obtain the platform orientation in the geographic coordinate system (this applies to both stationary and mobile sonar platforms).

.
[[coordinate_system_figure_all]]
image::coordinateSystemFigureAll.svg[align="center"]



=== Groups

The top-level group (labelled "/" in netCDF4 terminology) contains metadata about the NVI-netCDF4 file, represented as attributes in the group (<<topLevelTable>>).

[.landscape]
<<<
.Description of the top-level group.
[[topLevelTable]]
include::tableToplevel.adoc[]
[.portrait]
<<<


==== Platform group

This group contains information about the sonar platform (e.g. ship or similar). The netCDF4 group name is */Platform* and is described in <<platformGroupTable>>. Optionally, subgroups of /Platform can be used to store metadata and data from individual instruments that provide description or eventually measurements about the platform.

Each platform can contain several instruments of the same type, such as multiple position sensors. Each instrument type description and optionnaly raw data is defined as a subgroup of the /Platform group and data for each instrument can stored in a subgroup of the instrument group. For example, data from individual position and attitude instruments are stored as subgroups of the /Platform/Position and /Platform/Attitude subgroups. The names of those subgroups must match the names of the instrument's serial number or identifier variables in the /Platform group (<<platformGroupTable>>).  Additional instrument type subgroups can be created as required in order to store raw data for example.


Each instrument group can also store unprocessed and unparsed sensor data. The data format for unprocessed and unparsed data is not prescribed, but could, for example, be NMEA-style text and/or numeric values. These subgroups must have one attribute called “description” that provides a short description of the contents. Other attributes can be added as desired. The variables under the subgroup should have appropriate names and an attribute that gives the units, where appropriate.

As a general example, parsed data from a GPS with id _Garmin1234_ would be stored in /Platform/Position/Garmin1234 and if NMEA data from that GPS were also stored, it would be in /Platform/Position/Garmin1234/NMEA.

The coordinate system convention used for the /Platform group variables is detailed in <<coordinate_systems>>.

All instruments have a common core of attributes and definitions described in table (<<genericSensorTable>>). This common description contains names, and generic descriptions of a instrument and the characteristics of its installation on the platform
.

[.landscape]
<<<
.Description of generic sensor.
[[genericSensorTable]]
include::tableSensor.adoc[]

[.landscape]
<<<
.Description of the platform group.
[[platformGroupTable]]
include::tablePlatform.adoc[]

.Suggested subgroup names for platform instruments.
[cols=",,",options="header",]
[[platformInstrumentsTable]]
|===
|Sensor or datagram |Subgroup name | Comment
|Attitude sensors, MRU |Attitude | Use data structure described in <<AttitudeSubGroup>>
|GPS sensors, Position |Position | Use data structure described in <<PositionSubGroup>>
|Clock or time synchronisation datagrams | Clock |
|NMEA telegrams | NMEA | Use data structure described in <<NMEATable>>.
|===

.Description of a platform attitude subgroup.
[[AttitudeSubGroup]]
include::tableAttitude_sub_group.adoc[]

.Description of a platform position subgroup.
[[PositionSubGroup]]
include::tablePosition_sub_group.adoc[]

.Suggested group for storing NMEA datagrams from marine instruments.
[[NMEATable]]
include::tableNMEA.adoc[]
[.portrait]
<<<

[.landscape]
<<<
==== Provenance group

The provenance group provides information on how the SONAR-netCDF4 version of the data were created and what processes were applied to this data. The netCDF4 group name is */Provenance* and is described in <<provenanceTable>>.

.Description of the provenance group.
[[provenanceTable]]
include::tableProvenance.adoc[]
[.portrait]
<<<

==== Navigation group

The netCDF4 group name is */Navigation* and contains processed navigation data and metadata at the same sampling time.

[.landscape]
<<<
.Description of the sonar group.
[[navigationGroupTable]]
include::tableNavigation.adoc[]


==== Vendor specific group

The vendor specific group contains information about sensor the and data specific to the sensor. The contents of this group are at the discretion of the sensor and software that writes the NVI-netCDF4 file. The netCDF4 group name is */Vendor_specific*. There is no mandatory information for the vendor specific group.

.Description of the vendor specific group.
[[vendorSpecificTable]]
include::tableVendorSpecific.adoc[]



== Revision history
[[revision_history]]



=== Changes within a document version 

[cols=",,,",options="header",]
|===
|Document version |NVI version |Date |Changes
|1.0 |1.0 |15 May 2022 |Draft version for distribution”.
|===

== Acknowledgements



== References

bibliography::[]

:sectnums!:
== Annex 1: Working with netCDF4 files

This section provides simple examples of how to access and use netCDF4 files in Python. Xarray library is also a well known library for netcdf file parsing and is well adapted for netCDF4-NVI files.

.Python example
[source,python]
----
# Import packages needed to read and view data from netcdf4
from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt

# Name of the netcdf file
filename = 'NVI.nc'

# Open the file
dataset = Dataset(filename)


----
