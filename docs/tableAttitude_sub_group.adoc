:var: {nbsp}{nbsp}{nbsp}{nbsp}
:attr: {var}{var}
[cols="25%,10%,65%",options="header",]
|===
|Description |Obligation |Comment
s|Group attributes | |
 |{attr}string :description |M |System description, brand and type
 |{attr}string :system_name |M |A unique name for the sensor
 |{attr}string :device_category |M |Device category, name issued from https://vocab.seadatanet.org/v_bodc_vocab_v2/search.asp?lib=L05
 |{attr}string :constructor |O |System constructor
 |{attr}string :firmware_version |O |Comma separated list of system firmware version
 |{attr}string :software_version |O |Comma separated list of system software version
 |{attr}string :system_serial_number |O |system serial number, if needed can be a comma separated list of serial numbers
 
|===
